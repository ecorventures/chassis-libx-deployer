'use strict'

const os = require('os')
const fs = require('fs')
const path = require('path')
const cp = require('child_process')
const EventEmitter = require('events').EventEmitter

/**
* @class NPM
* Represents NPM activities
*/
class NPM extends EventEmitter {
  constructor (user, token) {
    super()

    Object.defineProperties(this, {
      cwd: {
        enumerable: false,
        writable: true,
        configurable: false,
        value: path.join(os.tmpdir(), '_npm_module_')
      },
      timer: {
        enumerable: false,
        writable: true,
        configurable: false,
        value: null
      },
      user: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: user
      },
      token: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: token
      },
      pkg: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: {}
      },
      readme: {
        enumerable: false,
        writable: true,
        configurable: false,
        value: ''
      }
    })

    try {
      fs.accessSync(this.cwd, fs.F_OK)
      del.sync(this.cwd)
    } catch (e) {}
    fs.mkdirSync(this.cwd)
  }

  notify (msg) {
    this.emit('status', msg)
  }

  publish (callback) {
    const me = this
    const pkg = require(path.join(this.cwd, 'package.json'))

    this.notify('Publishing ' + pkg.name + '@' + pkg.version)

    // fs.writeFileSync('~/.npmrc', '//registry.npmjs.org/:_authToken=' + this.token, {encoding: 'utf8'})
    // console.log(cp.execSync('echo //registry.npmjs.org/:_authToken=' + this.token + ' > ~/.npmrc').toString())
    // this.notify('Acting as ' + cp.execSync('npm whoami').toString())
    this.notify('Publish Manifest: ' + cp.execSync('ls -l').toString())

    // console.log(cp.execSync('chmod +x ' + path.join(__dirname, 'npm.sh')).toString())
    // this.notify('Executing Command: ' + path.join(__dirname, 'npm.sh') + me.cwd + ' ' + me.user + ' ' + me.token)
    const child = cp.spawn('npm', ['publish'], {
      cwd: this.cwd
    })

    me.timer = setTimeout(function () {
      callback(pkg.name + '@' + pkg.version + ' publishing timed out after 90 seconds.')
      setTimeout(function () {
        process.exit(1)
      }, 1000)
    }, 90000)

    let data = ""
    child.stdout.on('data', function (chunk) {
      data += chunk.toString()
    })

    child.stderr.on('data', function (chunk) {
      console.log('[STDERR]', chunk.toString())
      data += chunk.toString()
    })

    child.on('close', function (exitcode) {
      clearTimeout(me.timer)
      if (data.indexOf('cannot publish over') >= 0) {
        callback(pkg.name + '@' + pkg.version + ' already exists.')
      } else if (data.indexOf('publish Failed') >= 0) {
        me.notify('[ERROR DETAIL] ' + data)
        callback('Publish failed. Please see the log for detail.')
      } else {
        me.notify('Publish complete.')
        callback(null)
      }
    })
  }

  set (variable, value) {
    this.pkg[variable] = value
  }

  addFile (source, targetPath) {
    this.notify('Adding ' + source + ' to ' + targetPath)
    fs.renameSync(source, path.join(this.cwd, targetPath))
  }

  build (callback) {
    this.notify('Building module...')

    // Write package file
    fs.writeFileSync(path.join(this.cwd, 'package.json'), JSON.stringify(this.pkg, null, 2), {
      encoding: 'utf8'
    })

    // Create README
    if (this.readme.trim().length > 0) {
      fs.writeFileSync(path.join(this.cwd, 'README.md'), this.readme, {
        encoding: 'utf8'
      })
    }

    callback()
  }
}

module.exports = NPM
